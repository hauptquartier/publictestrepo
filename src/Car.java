/**
 * Created on 13.02.2017.
 */
public class Car {
    private String manufacturer;
    private String model;
    private int horsepower;

    public Car(String manufacturer, String model, int horsepower) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.horsepower = horsepower;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }
}
